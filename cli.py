#!./.venv/bin/python

import os
import typer
from typing import Optional
from enum import Enum

app = typer.Typer()


class Environment(str, Enum):
    dev = "dev"
    test = "test"
    staging = "staging"
    production = "production"


@app.command()
def serve(env: Optional[Environment] = Environment.dev, host: str = "0.0.0.0"):
    typer.echo(f"\nRunning Auth Service API | Environment: {env} 🚀 \n")
    os.system(f"ENV_FILE='.env.{env}' uvicorn src.main:service --reload --host {host}")


@app.command("venv")
def create_venv():
    typer.echo("\nCreating virtual environment 🍇")
    os.system("python -m venv .venv")
    command = typer.style(
        "`source ./.venv/bin/activate`", fg=typer.colors.GREEN, bold=True
    )
    typer.echo(f"\nActivate with: {command}. Happy coding 😁 \n")


@app.command()
def install():
    typer.echo("\nInstalling packages 🚀")
    os.system("pip install -r src/requirements.txt ")
    typer.echo(f"\nPackages installed. Have fun 😁 \n")


@app.command()
def test(watch: bool = False):
    typer.echo("\nTesting package 🚀")
    if watch:
        os.system("ENV_FILE='.env.test' ptw")
        return
    os.system("ENV_FILE='.env.test' pytest")


@app.command()
def test_runner():
    typer.echo("\nTesting gitlab-runner 🚀")
    os.system("gitlab-runner exec docker api")


if __name__ == "__main__":
    app()
