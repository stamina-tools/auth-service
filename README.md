# Auth service

Auth Service

Auth service is offered as a microservice that keeps users and sessions in the form of JWT. The service is unaware of external systems and only provides routes for producing JWTs, getting user info, cleaning JWTs (logging out) and relevant functionality.

In the architecture the microservice can be placed in 2 different spots:

- Behind the gateway. This makes all other services unaware of this service's existence, thus it is the work of the gateway to check and validate the auth in every call before proxying it to the needed service. All other services have to be inside a VPN.
- As a single source of Auth for every service in the realm. This requires that every service that needs to perform an operation validates that the user asking for the action is correctly authenticated. The downside of this approach is that the communication can get a little chatty, plus every service will need to implement an Auth middleware.
  For better distribution there is also a Dockerfile included.

You can also find documentation on the routes (with Swagger) in the `/docs` route. For example `http://0.0.0.0:8000/docs`

To start working with the service after cloning, create a python virtual environment:

```
python -m venv .venv
```

You can list available CLI commands with:

```
./cli.py --help
```

To start the server:

```
./cli.py serve
```

To start the tests:

```
./cli.py test
```

To watch the tests:

```
./cli.py test --watch
```
