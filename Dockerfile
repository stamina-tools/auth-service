FROM python:3.7

# We copy just the requirements.txt first to leverage Docker cache
COPY src/requirements.txt src/requirements.txt

RUN pip install -r src/requirements.txt

COPY . .

ENTRYPOINT python wyse_cli.py serve --env staging