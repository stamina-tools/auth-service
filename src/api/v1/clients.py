from src.core import services
from src.core.settings import get_settings
from fastapi import APIRouter, Depends
from src.db.connection import AsyncIOMotorDatabase, get_database
from src.schemas.client import Client, ClientInCreate, ClientInDb
from src import crud

clients_router = APIRouter()
settings = get_settings()


@clients_router.post(
    "/clients/sign-up",
    tags=["Clients"],
    summary="Create new client",
    response_model=Client,
)
async def sign_up_client(
    db: AsyncIOMotorDatabase = Depends(get_database),
    *,
    org_in_create: ClientInCreate,
) -> ClientInDb:
    org_in_create.password = services.auth.get_password_hash(org_in_create.password)
    return await crud.client.create(db, item_in_create=org_in_create)
