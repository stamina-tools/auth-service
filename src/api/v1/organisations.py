from src.core import services
from typing import List
from src.schemas.organisation import (
    Organisation,
    OrganisationInCreate,
)
from src.core.settings import get_settings
from fastapi import APIRouter, Depends
from src.db.connection import AsyncIOMotorDatabase, get_database
from src import crud

organisations_router = APIRouter()
settings = get_settings()


@organisations_router.post(
    "/organisations",
    tags=["Organisations"],
    summary="Create new organisation",
    response_model=Organisation,
)
async def create_organisation(
    db: AsyncIOMotorDatabase = Depends(get_database),
    *,
    org_in_create: OrganisationInCreate,
):
    # Check for duplicates
    await services.organisations.check_org_duplicate_names(db, org_in_create.name)
    return await crud.organisation.create(db, item_in_create=org_in_create)


@organisations_router.get(
    "/organisations",
    tags=["Organisations"],
    summary="Get all organisations",
    response_model=List[Organisation],
)
async def get_organisations(db: AsyncIOMotorDatabase = Depends(get_database)):
    return await crud.organisation.find_many(db)
