from src.schemas.utils import ClientError
from src.schemas.auth import OAuth2RequestForm, Token, TokenData
from fastapi import Depends, APIRouter, HTTPException
from src.db.connection import get_database
from motor.motor_asyncio import AsyncIOMotorDatabase
from src.schemas.client import Client, ClientInDb
from src.core import services

auth_router = APIRouter()


@auth_router.post(
    "/auth/token",
    tags=["Auth"],
    summary="Request Auth token",
    response_description="Auth token containing the JWT and the token type",
    response_model=Token,
    responses={404: {"model": ClientError}},
)
async def authenticate(
    form_data: OAuth2RequestForm = Depends(),
    db: AsyncIOMotorDatabase = Depends(get_database),
):
    """
    This route returns a token to gain access to the private API routes. This endpoint
    can also be used to obtain a new token when the access_token has expired.
    """
    client_in_db = None
    # On api scope authenticate by client keys
    if "api" in form_data.scopes:
        client_in_db = await services.auth.authenticate_by_client_keys(
            db, client_id=form_data.client_id, client_secret=form_data.client_secret
        )
    if "dashboard" in form_data.scopes:
        client_in_db = await services.auth.authenticate_by_credentials(
            db, email=form_data.email, password=form_data.password
        )

    if not client_in_db:
        raise HTTPException(status_code=404, detail="No client found")

    token_data = TokenData(**client_in_db.dict())
    access_token = services.auth.create_access_token(
        data={"subject": token_data.json(), "scopes": form_data.scopes}
    )

    return Token(access_token=access_token, token_type="Bearer")


@auth_router.get(
    "/auth/me",
    tags=["Auth"],
    summary="Get current authenticated API user",
    response_description="Client using the token",
    response_model=Client,
    responses={404: {"model": ClientError}},
)
async def get_authenticated_client(
    active_client: ClientInDb = Depends(services.auth.find_active_client),
) -> ClientInDb:
    """
    This routes returns the user the provided token belongs to.
    Can be used to access client information.
    """
    return active_client
