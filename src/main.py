from src.schemas.errors import NotFoundError
from fastapi import FastAPI, Request
from src.db.connection import connect_to_mongo, close_mongo_connection
from src.core.settings import get_settings
from fastapi.middleware.cors import CORSMiddleware
from src.api.v1 import v1_router
from fastapi.responses import JSONResponse

settings = get_settings()
service = FastAPI(title=settings.APP_NAME, version="1.0.0")


def log_environment():
    settings = get_settings()
    print(f"LOG INFO: Environment: {settings.ENV}")


service.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

service.add_event_handler("startup", log_environment)
service.add_event_handler("startup", connect_to_mongo)
service.add_event_handler("shutdown", close_mongo_connection)


@service.exception_handler(NotFoundError)
async def unicorn_exception_handler(request: Request, exc: NotFoundError):
    return JSONResponse(status_code=404, content={"detail": exc.detail})


service.include_router(v1_router)
