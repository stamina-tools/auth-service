import json
from src.tests.conftest import DataHolder
from src.tests.mock_values import (
    client_mock,
    api_auth_credentials_mock,
    dashboard_auth_credentials_mock,
)


"""
API E2E tests

This E2E flow tries to emulate real life examples of calls in a happy case scenario.
The purpose of this is to test the following:
    * Happy case scenarios where the api user does expected calls.
    * Database connection
    * Validation

To achieve this the tests follow the below con:
    * Database connections is real (data are written to a db and then the db is killed)
    * External service calls (providers, etc) are mocked on the service level
    * Data are shared between the tests so that we can simulate a real flow

Note: Keep order of functions because it tries to simulate the normal procedure
"""


def test_sign_up(client):
    data = json.dumps(client_mock)
    response = client.post("v1/clients/sign-up", data=data)
    new_client = response.json()

    assert response.status_code == 200
    assert new_client["email"] == client_mock["email"]
    assert new_client["roles"] == ["admin"]

    DataHolder.client = new_client


def test_api_auth(client):
    response = client.post("v1/auth/token", data=api_auth_credentials_mock)

    assert response.status_code == 200

    DataHolder.access_token = f"Bearer {response.json()['access_token']}"
    DataHolder.headers = {"Authorization": DataHolder.access_token}


def test_dashboard_auth(client):
    response = client.post("v1/auth/token", data=dashboard_auth_credentials_mock)

    assert response.status_code == 200


def test_me(client):
    response = client.get("v1/auth/me", headers=DataHolder.headers)

    assert response.status_code == 200
    assert response.json()["email"] == DataHolder.client["email"]
