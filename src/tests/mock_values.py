client_mock = {
    "password": "string",
    "email": "string@string.com",
    "client_id": "QlQUpvJFU2SuVBf2nwXkt3QMZJuGm2j9HuorfIxurRQz",
    "client_secret": "CTTJ0A0A_rInLlOYYVf2mIShrWE9FXR_DYaM_GKCtUgz",
    "roles": ["admin"],
}


api_auth_credentials_mock = {
    "scope": "api",
    "client_id": client_mock["client_id"],
    "client_secret": client_mock["client_secret"],
}


dashboard_auth_credentials_mock = {
    "scope": "dashboard",
    "email": client_mock["email"],
    "password": client_mock["password"],
}