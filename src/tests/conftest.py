from typing import Any
from pytest import fixture
from fastapi.testclient import TestClient
from src.db.connection import get_connection_client
import asyncio
from src.main import service
from src.core.settings import get_settings

settings = get_settings()


@fixture(scope="session")
def client():
    with TestClient(service) as client:
        yield client
    conn = asyncio.run(get_connection_client())
    conn.drop_database(settings.DB_NAME)


class DataHolder:
    client: Any = None
    access_token: Any = None
    headers: Any = None
