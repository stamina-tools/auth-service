from typing import Union
from .base import Crud
from src.schemas.client import ClientBase, ClientInDb, ClientPreDb, ClientInDb


class ClientRepository(Crud[ClientBase, ClientPreDb, ClientInDb, ClientBase]):
    async def find_by_client_secret(
        self, db, *, client_secret: str
    ) -> Union[ClientInDb, None]:
        return await self.find_one(db, filter={"client_secret": client_secret})

    async def find_by_client_id(self, db, *, client_id: str) -> Union[ClientInDb, None]:
        return await self.find_one(db, filter={"client_id": client_id})

    async def find_by_username(self, db, *, email: str) -> Union[ClientInDb, None]:
        return await self.find_one(db, filter={"email": email})


client = ClientRepository(
    collection="client", ItemPreDb=ClientPreDb, ItemInDb=ClientInDb,
)
