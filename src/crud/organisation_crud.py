from .base import Crud
from src.schemas.organisation import (
    OrganisationBase,
    OrganisationInDb,
    OrganisationPreDb,
    OrganisationInDb,
)


class OrganisationRepository(
    Crud[OrganisationBase, OrganisationPreDb, OrganisationInDb, OrganisationBase]
):
    pass


organisation = OrganisationRepository(
    collection="organisation",
    ItemPreDb=OrganisationPreDb,
    ItemInDb=OrganisationInDb,
)
