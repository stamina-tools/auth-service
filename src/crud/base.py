import datetime
from src.schemas.errors import NotFoundError
from motor.motor_asyncio import AsyncIOMotorClient
from bson.objectid import ObjectId
from typing import Any, Dict, Generic, Optional, List, Type, TypeVar, Union
from bson import SON
from src.schemas.base import ItemInDb, ItemPreDb, ItemBase


ItemPreDbType = TypeVar("ItemPreDbType", bound=ItemPreDb)
ItemInDbType = TypeVar("ItemInDbType", bound=ItemInDb)
ItemInUpdateType = TypeVar("ItemInUpdateType", bound=ItemBase)
ItemBaseType = TypeVar("ItemBaseType", bound=ItemBase)


class Crud(Generic[ItemBaseType, ItemPreDbType, ItemInDbType, ItemInUpdateType]):
    """
    Basic crud interactions with database in an async manner.
    Functions will raise if the document is not found by default. This can be overwritten.
    """

    def __init__(
        self,
        *,
        collection: str,
        ItemPreDb: Type[ItemPreDbType],
        ItemInDb: Type[ItemInDbType],
        # Not all items are updatable
        ItemInUpdate: Optional[ItemInUpdateType] = None,
    ):
        self.collection = collection
        # Non instantiated pydratic models. Used for validation.
        self.ItemInDb = ItemInDb
        self.ItemPreDb = ItemPreDb
        self.ItemInUpdate = ItemInUpdate

    async def create(
        self, db: AsyncIOMotorClient, *, item_in_create: ItemBaseType
    ) -> ItemInDbType:
        item_pre_db = self.ItemPreDb(**item_in_create.dict())
        row = await db[self.collection].insert_one(SON(item_pre_db.dict()))
        return self.ItemInDb(id=row.inserted_id, **item_pre_db.dict())

    async def create_many(
        self, db: AsyncIOMotorClient, *, items_in_create: List[ItemBaseType]
    ) -> List[ItemInDbType]:
        items_pre_db = [
            self.ItemPreDb(**item_in_create.dict())
            for item_in_create in items_in_create
        ]
        sons = [SON(item_pre_db.dict()) for item_pre_db in items_pre_db]
        result = await db[self.collection].insert_many(sons)
        items_in_db = [
            await self.find_by_id(db, id=str(id)) for id in result.inserted_ids
        ]
        return items_in_db

    async def update_by_id(
        self,
        db: AsyncIOMotorClient,
        *,
        id: str,
        item_in_update: ItemBase,
        should_raise_on_not_found: bool = True,
    ) -> ItemInDbType:
        filter = dict(_id=ObjectId(id))
        item_in_db = await self.find_by_id(db, id=id)
        item_in_update_dict = item_in_update.dict()
        replacement = {}
        for key, value in item_in_db.dict().items():
            replacement[key] = (
                item_in_update_dict[key] if key in item_in_update_dict else value
            )
        replacement["date_updated"] = datetime.datetime.now()
        del replacement["id"]
        updated_item_in_db = self.ItemInDb(id=ObjectId(id), **replacement)
        result = await db[self.collection].replace_one(
            filter=filter, replacement=replacement
        )
        return updated_item_in_db

    async def delete_by_id(
        self,
        db: AsyncIOMotorClient,
        *,
        id: str,
        should_raise_on_not_found: bool = True,
    ) -> ItemInDbType:
        filter = dict(_id=ObjectId(id))
        doc = await db[self.collection].find_one_and_delete(filter)
        return self.doc_or_raise(doc, should_raise_on_not_found)

    async def find_one(
        self,
        db: AsyncIOMotorClient,
        *,
        filter: Optional[Dict],
        should_raise_on_not_found: bool = True,
    ) -> ItemInDbType:
        doc = await db[self.collection].find_one(filter)
        return self.doc_or_raise(doc, should_raise_on_not_found)

    async def find_by_id(
        self,
        db: AsyncIOMotorClient,
        *,
        id: str,
        should_raise_on_not_found: bool = True,
    ) -> ItemInDbType:
        doc = await db[self.collection].find_one(ObjectId(id))
        return self.doc_or_raise(doc, should_raise_on_not_found)

    async def find_many(
        self,
        db: AsyncIOMotorClient,
        *,
        filter: Optional[Dict] = None,
        length: Optional[int] = 100,
    ) -> List[ItemInDbType]:
        cursor = db[self.collection].find(filter)
        item_list = []
        for doc in await cursor.to_list(length):
            item_in_db = self.ItemInDb(id=doc["_id"], **doc)
            item_list.append(item_in_db)
        return item_list

    def doc_or_raise(self, doc: Any, should_raise_on_not_found: bool,) -> ItemInDbType:
        if not doc:
            if should_raise_on_not_found:
                raise NotFoundError(detail=f"{self.collection.capitalize()} not found")
            else:
                # Doc is None here
                return doc
        else:
            return self.ItemInDb(id=doc["_id"], **doc)
