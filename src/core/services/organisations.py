from fastapi.exceptions import HTTPException
from starlette import status
from src import crud


async def check_org_duplicate_names(db, organisation_name):
    """
    Raises if organisation duplicates are found in the DB
    """
    organisations_in_db = await crud.organisation.find_many(db)
    org_with_same_name = next(
        org
        for org in organisations_in_db
        if org.name.lower() == organisation_name.lower()
    )
    if org_with_same_name:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Organisation with same name exists",
        )