from src.core.settings import get_settings
from motor.motor_asyncio import AsyncIOMotorDatabase
from src.schemas.auth import TokenData
from datetime import datetime, timedelta
from typing import List, Union
from fastapi import Depends, status, HTTPException
from fastapi.security.oauth2 import OAuth2PasswordBearer
from src.schemas.client import ClientInDb
from passlib.context import CryptContext
from jose import JWTError, jwt
import json
from src.db.connection import get_database
from src import crud

settings = get_settings()

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

scopes = dict(api="Access public api", dashboard="Access the dashboard")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="v1/auth/token", scopes=scopes)


class Auth:
    def verify_password(self, plain_password, hashed_password):
        return pwd_context.verify(plain_password, hashed_password)

    def get_password_hash(self, password):
        return pwd_context.hash(password)

    def verify_client_secret(
        self, provided_client_secret: str, real_client_secret: str
    ) -> bool:
        return provided_client_secret == real_client_secret

    async def authenticate_by_client_keys(
        self, db, client_id: str, client_secret: str
    ) -> Union[ClientInDb, None]:
        """Used for api scopes where user is authenticated by client keys only."""
        client_in_db = await crud.client.find_by_client_id(db, client_id=client_id)
        if not self.verify_client_secret(client_secret, client_in_db.client_secret):
            return None
        return client_in_db

    async def authenticate_by_credentials(
        self, db, email: str, password: str
    ) -> Union[ClientInDb, None]:
        """Used for dashboard scopes where user is authenticated by credentials."""
        client_in_db = await crud.client.find_by_username(db, email=email)
        if not self.verify_password(password, client_in_db.password):
            return None
        return client_in_db

    def create_access_token(self, data: dict) -> str:
        to_encode = data.copy()
        minutes = settings.ACCESS_TOKEN_EXPIRE_MINUTES
        expire = datetime.utcnow() + timedelta(minutes=minutes)
        to_encode.update({"exp": expire})
        encoded_jwt = jwt.encode(
            to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM
        )
        return encoded_jwt

    async def find_active_client(
        self,
        db: AsyncIOMotorDatabase = Depends(get_database),
        token: str = Depends(oauth2_scheme),
    ) -> ClientInDb:
        # TODO: there is some duplicated logic here. Also maybe this can be simplified.
        credentials_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )

        try:
            payload = jwt.decode(
                token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
            )
            token_sub: str = payload.get("subject")
            token_scopes: List[str] = payload.get("scopes")
            token_data = TokenData(**json.loads(token_sub))
            if not "api" in token_scopes and not "dashboard" in token_scopes:
                raise credentials_exception
        except JWTError as e:
            print(e)
            raise credentials_exception

        client_in_db = None
        if "api" in token_scopes:
            client_in_db = await crud.client.find_by_client_id(
                db, client_id=token_data.client_id
            )
        if "dashboard" in token_scopes:
            client_in_db = await crud.client.find_by_username(
                db, email=token_data.email
            )

        if client_in_db is None:
            raise credentials_exception
        return client_in_db


auth = Auth()
