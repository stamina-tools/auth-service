from pydantic import BaseSettings
from functools import lru_cache
import os


class Settings(BaseSettings):
    APP_NAME: str
    ENV: str

    DB_NAME: str
    MONGO_URL: str

    SECRET_KEY: str
    ALGORITHM: str
    ACCESS_TOKEN_EXPIRE_MINUTES: int


    class Config:
        env_file = os.getenv("ENV_FILE") or ".env.dev"


@lru_cache()
def get_settings():
    return Settings()
