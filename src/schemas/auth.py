from fastapi.params import Form
from typing import Optional
from pydantic import BaseModel


class OAuth2RequestForm:
    def __init__(
        self,
        grant_type: str = Form(None),
        email: Optional[str] = Form(None),
        password: Optional[str] = Form(None),
        scope: str = Form(...),
        client_id: Optional[str] = Form(None),
        client_secret: Optional[str] = Form(None),
    ):
        self.grant_type = grant_type
        self.email = email
        self.password = password
        self.scopes = scope.split() if scope else []
        self.client_id = client_id
        self.client_secret = client_secret


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Optional[str] = None
    client_id: str = None
