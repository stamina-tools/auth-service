from bson.objectid import ObjectId
import datetime
from pydantic import BaseModel


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not isinstance(v, ObjectId):
            raise TypeError("ObjectId required")
        return str(v)


class ItemBase(BaseModel):
    """Base entity"""

    pass


class ItemPreDb(ItemBase):
    """Must initialise dates."""

    date_updated: datetime.datetime = datetime.datetime.now()
    date_created: datetime.datetime = datetime.datetime.now()


class ItemInDb(ItemBase):
    """Must have an id and dates. Rest of properties come from base."""

    id: PydanticObjectId
    date_updated: datetime.datetime
    date_created: datetime.datetime
