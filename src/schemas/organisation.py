from typing import List
from pydantic import EmailStr
import secrets
from .base import ItemBase, ItemInDb, ItemPreDb


class OrganisationBase(ItemBase):
    name: str
    country: str


class OrganisationInCreate(OrganisationBase):
    name: str
    country: str


class OrganisationPreDb(OrganisationBase, ItemPreDb):
    pass


class OrganisationInDb(OrganisationPreDb, ItemInDb):
    pass


class Organisation(OrganisationInDb):
    id: str
