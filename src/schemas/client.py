from typing import List, Optional
from pydantic import EmailStr
import secrets
from .base import ItemBase, ItemInDb, ItemPreDb


class ClientBase(ItemBase):
    email: EmailStr
    password: str
    client_id: str
    client_secret: str
    roles: List[str]
    organisation_id: Optional[str]


class ClientInCreate(ClientBase):
    client_id: str = secrets.token_urlsafe(32)
    client_secret: str = secrets.token_urlsafe(32)
    roles: List[str] = []


class ClientPreDb(ClientBase, ItemPreDb):
    pass


class ClientInDb(ClientPreDb, ItemInDb):
    pass


class Client(ClientInDb):
    id: str
