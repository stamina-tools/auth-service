from motor.motor_asyncio import AsyncIOMotorClient
from motor.motor_asyncio import AsyncIOMotorDatabase
from src.core.settings import get_settings

settings = get_settings()


class DataBase:
    client: AsyncIOMotorClient = None


db = DataBase()


async def connect_to_mongo():
    print(f"LOG INFO: Connecting to mongo: {settings.MONGO_URL}")
    db.client = AsyncIOMotorClient(settings.MONGO_URL)
    print("LOG INFO: Connected")


async def close_mongo_connection():
    print("LOG INFO: Disconnecting from mongo")
    db.client.close()
    print("LOG INFO: Disconnected")


async def get_database() -> AsyncIOMotorDatabase:
    return db.client[settings.DB_NAME]


async def get_connection_client() -> AsyncIOMotorClient:
    return db.client
